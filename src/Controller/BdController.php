<?php

namespace App\Controller;

use App\Repository\BDRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BdController extends AbstractController
{
    /**
     * @Route("/bd", name="bd")
     */
    public function index(BDRepository $repo)
    {
        return $this->render('bd/index.html.twig', [
            'BDs' => $repo->findAll()
        ]);
    }

    // /**
    //  * @Route("/add-bd", name="add_bd")
    //  */
    // public function addBd(Request $request, objectifManager $manager){
    //     $bd = new BD();
    //     $form = $this->createForm();
    // }
}
