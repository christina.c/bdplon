<?php

namespace App\DataFixtures;

use App\Entity\BD;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct (UserPasswordEncoderInterface $encoder){

         $this->encoder = $encoder;

    }
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 5; $i++) {
            $user = new User();
            $hashedPassword = $this->encoder->encodePassword($user, '1234');

            $user->setUsername('username' .$i)
                ->setPassword($hashedPassword)
                ->setRoles(['ROLE_USER']);
                $manager->persist($user);
        }

        for ($x = 1; $x < 5; $x++) {
            $bd = new BD();
            $datetime = new DateTime();

            $bd->setTitle('Title '.$x)
                ->setAuthor('Author' .$x)
                ->setDescription('Voici la déscription de la bd '.$x)
                ->setParution($datetime)
                ->setImgPath('/home/chabert/html/bdplon/public/tamarra.jpeg');
                $manager->persist($bd);
         }

        $manager->flush();
    }
}
